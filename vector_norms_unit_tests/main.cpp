#include <iostream>
#include <limits>
#include <stdexcept>
#include <exception>
#include "large_vector.hpp"
#include "interval.hpp"

int main(int argc, const char * argv[]) {
    
    try {
        std::cout << "#################### Tests for Interval. ####################\n";
        
        int correct;
        Interval* test_interval;
        
        //1
        std::cout << "1. Constructor and Get test. start = 4, end = 6.";
        try {
            test_interval = new Interval(4, 6);
        } catch (std::bad_alloc& ba) {
            std::string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw std::runtime_error(er);
        }
        if (test_interval->GetStart() != 4 ||
            test_interval->GetEnd() != 6) {
            std::cout << " NOT PASSED\n";
            throw std::runtime_error("1. Constructor or Get of interval is wrong.");
        }
        delete test_interval;
        std::cout << " PASSED\n";
        
        //2
        std::cout << "2. Constructor and Get test. start = 5, end = 5.";
        try {
            test_interval = new Interval(5, 5);
        } catch (std::bad_alloc& ba) {
            std::string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw std::runtime_error(er);
        }
        if (test_interval->GetStart() != 5 ||
            test_interval->GetEnd() != 5) {
            std::cout << " NOT PASSED\n";
            throw std::runtime_error("1. Constructor or Get of interval is wrong.");
        }
        delete test_interval;
        std::cout << " PASSED\n";
        
        //3
        std::cout << "3. Constructor test with interval [6;5].";
        try {
            correct = 0;
            test_interval = new Interval(6, 5);
        } catch (std::bad_alloc& ba) {
            std::string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw std::runtime_error(er);
        } catch (const std::invalid_argument& excp) {
            correct = 1;
        }
        if (correct == 0) {
            std::cout << " NOT PASSED\n";
            throw std::runtime_error("3. Constructor is wrong.");
        }
        std::cout << " PASSED\n";
        
        std::cout << "#################### Tests for IndicesStorage. ####################\n";
        
        IndicesStorage* test_storage;
        
        //1
        std::cout << "1. Blank constructor test.";
        try {
            test_storage = new IndicesStorage();
        } catch (std::bad_alloc& ba) {
            std::string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw std::runtime_error(er);
        }
        if (test_storage->GetStatus() != BAD ||
            test_storage->GetSize() != 0 ||
            test_storage->GetRecall(Interval(0,0)) != false) {
            std::cout << " NOT PASSED\n";
            throw std::runtime_error("1. Blank Constructor of IndicesStorage is wrong.");
        }
        delete test_storage;
        std::cout << " PASSED\n";
        
        //2
        std::cout << "2. Constructor test. Interval [1;20] to 10 parts";
        try {
            test_storage = new IndicesStorage(Interval(1, 20), 10);
        } catch (std::bad_alloc& ba) {
            std::string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw std::runtime_error(er);
        }
        if (test_storage->GetStatus() != OK ||
            test_storage->GetSize() != 10 ||
            test_storage->GetRecall(Interval(1,20)) != true ||
            test_storage->GetBoundaries().GetStart() != 1 ||
            test_storage->GetBoundaries().GetEnd() != 20) {
            std::cout << " NOT PASSED\n";
            throw std::runtime_error("2. Constructor of IndicesStorage is wrong.");
        }
        for (int i = 0; i < 10; i++) {
            if ((*test_storage)[i].GetStart() != 2 * i + 1 ||
                (*test_storage)[i].GetEnd() != 2 * i + 2 ||
                (*test_storage)[i].GetSize() != 2) {
                std::cout << " NOT PASSED\n";
                throw std::runtime_error("2. Constructor of IndicesStorage is wrong.");
            }
        }
        delete test_storage;
        std::cout << " PASSED\n";
        
        
        //3
        std::cout << "3. Constructor test. Interval [1;10] to 20 parts";
        try {
            test_storage = new IndicesStorage(Interval(1, 10), 20);
        } catch (std::bad_alloc& ba) {
            std::string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw std::runtime_error(er);
        }
        if (test_storage->GetStatus() != OK ||
            test_storage->GetSize() != 10 ||
            test_storage->GetRecall(Interval(1,10)) != true ||
            test_storage->GetBoundaries().GetStart() != 1 ||
            test_storage->GetBoundaries().GetEnd() != 10) {
            std::cout << " NOT PASSED\n";
            throw std::runtime_error("3. Constructor of IndicesStorage is wrong.");
        }
        for (int i = 0; i < 10; i++) {
            if ((*test_storage)[i].GetStart() != i + 1 ||
                (*test_storage)[i].GetEnd() != i + 1 ||
                (*test_storage)[i].GetSize() != 1) {
                std::cout << " NOT PASSED\n";
                throw std::runtime_error("3. Constructor of IndicesStorage is wrong.");
            }
        }
        delete test_storage;
        std::cout << " PASSED\n";
        
        std::cout << "#################### Tests for LargeVector. ####################\n";
        
        LargeVector<norm_t>* test_vector;
        
        //1
        std::cout << "1. Simple max norm test.";
        try {
            test_vector = new LargeVector<norm_t>(10);
        } catch (std::bad_alloc& ba) {
            std::string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw std::runtime_error(er);
        }
        if (test_vector->GetMaxNorm() != 9) {
            std::cout << " NOT PASSED\n";
            throw std::runtime_error("1. LargeVector max norm is wrong.");
        }
        delete test_vector;
        std::cout << " PASSED\n";
        
        //2
        std::cout << "2. Simple euq norm test.";
        try {
            test_vector = new LargeVector<norm_t>(10);
        } catch (std::bad_alloc& ba) {
            std::string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw std::runtime_error(er);
        }
        if (test_vector->GetEuqNorm() != sqrt((norm_t)(1+4+9+16+25+36+49+64+81))) {
            std::cout << " NOT PASSED\n";
            throw std::runtime_error("1. LargeVector max norm is wrong.");
        }
        delete test_vector;
        std::cout << " PASSED\n";
        
        
        
        std::cout << "\nALL TESTS ARE PASSED\n";
    } catch (const std::runtime_error& re) {
        std::cout << "Runtime error: " << re.what() << std::endl;
    } catch (const std::exception& ex) {
        std::cout << "Error occurred: " << ex.what() << std::endl;
    } catch (...) {
        std::cout << "Unknown failure occurred. Possible memory corruption" << std::endl;
    }
    
    return 0;
}
